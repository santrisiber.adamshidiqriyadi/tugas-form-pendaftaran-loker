<!-- database tugas = seminar
tabel  seminar = peserta
tampilan    =   data_peserta
 -->
<?php 

$conn = mysqli_connect("localhost", "root", "", "loker");


function query($query){
	global $conn;
	$result = mysqli_query($conn, $query);
	$rows = [];
	while ( $row = mysqli_fetch_assoc($result) ) {
		$rows[] = $row;
	}
	return $rows;
}




function tambah($data) {
	global $conn;

	//ambil data dari tiap elemen dalam form
	$id = htmlspecialchars($data["id"]);
	$nama = htmlspecialchars($data["nama"]);
	$email = htmlspecialchars($data["email"]);
	$no_telpon = htmlspecialchars($data["no_telpon"]);
	$tempat_lahir = htmlspecialchars($data["tempat_lahir"]);
	$tanggal_lahir = htmlspecialchars($data["tanggal_lahir"]);
	$jenis_instansi = $data["jenis_instansi"];
	$nama_instansi = $data["nama_instansi"];


	// query insert data
	$query = "INSERT INTO karyawan
				VALUES
				('', '$id', '$nama', '$email', '$no_telpon', '$tempat_lahir', '$tanggal_lahir', '$jenis_instansi', '$nama_instansi')
				";
	mysqli_query($conn, $query);

	return mysqli_affected_rows($conn);
}



function hapus($id) {
	global $conn;

	mysqli_query($conn, "DELETE FROM karyawan WHERE id = $id");

	return mysqli_affected_rows($conn);
}



function ubah($data) {
	global $conn;

	//ambil data dari tiap elemen dalam form
	$id = htmlspecialchars($data["id"]);
	$nama = htmlspecialchars($data["nama"]);
	$email = htmlspecialchars($data["email"]);
	$no_telpon = htmlspecialchars($data["no_telpon"]);
	$tempat_lahir = htmlspecialchars($data["tempat_lahir"]);
	$tanggal_lahir = htmlspecialchars($data["tanggal_lahir"]);
	$jenis_instansi = $data["jenis_instansi"];
	$nama_instansi = $data["nama_instansi"];



	// query insert data
	$query = "UPDATE karyawan SET
				id = '$id',
				nama = '$nama',
				email = '$email',
				no_telpon = '$no_telpon',
				tempat_lahir = '$tempat_lahir',
				tanggal_lahir = '$tanggal_lahir',
				jenis_instansi = '$jenis_instansi',
				nama_instansi = '$nama_instansi',
			  WHERE id = $id
			";
	mysqli_query($conn, $query);

	return mysqli_affected_rows($conn);
}


function cari($keyword) {
	$query = "SELECT * FROM karyawan
				WHERE
			  id LIKE '%$keyword%' OR 
			  nama LIKE '%$keyword%' OR 
			  email LIKE '%$keyword%' OR
			  no_telpon LIKE '%$keyword%' OR 
			  tempat_lahir LIKE '%$keyword%' OR
			  tanggal_lahir LIKE '%$keyword%' OR 
			  jenis_instansi LIKE '%$keyword%' OR
			  nama_instansi LIKE '%$keyword%'
			  
			";

	return query($query);
}
