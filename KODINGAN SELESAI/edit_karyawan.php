<?php 
require 'function.php';

//ambil data di URL
$id = $_GET["id"];

// query data seminar berdasarkan id
$smr = query("SELECT * FROM karyawan WHERE id = $id")[0];

// cek apakah tombol submit sudah ditekan atau belum
if ( isset($_POST["submit"]) ) {

	// cek apakah data berhasil diedit atau tidak
	if( ubah($_POST) > 0 ) {
		echo "
			<script>
				alert('Data berhasil diedit');
				document.location.href = 'data_peserta.php';
			</script>
		";
	} else {
		echo "
			<script>
				alert('Data gagal diedit');
				document.location.href = 'data_peserta.php';
			</script>
		";
	}
}

?>
<html>
<head>
	<title>Edit Karyawan</title>
</head>
<body>
<h2> Edit Input Karyawan</h2>
<hr>
<form action="update_karyawan.php" method="POST">
	<input type="hidden" name="id" value="<?= $smr["id"] ?>">
<table>
	<tr>
				<td>ID</td>
				<td>:</td>
				<td><input type="text" name="id" id="id" required value="<?= $smr["id"] ?>"></td>
			</tr>
		<tr>
				<td>Nama</td>
				<td>:</td>
				<td><input type="text" name="nama" id="nama" required value="<?= $smr["nama"] ?>"></td>
			</tr>	
			<tr>
				<td>Email</td>
				<td>:</td>
				<td><input type="email" name="email" id="email" required value="<?= $smr["email"] ?>"></td>
			</tr>
			<tr>
				<td>No Telepon</td>
				<td>:</td>
				<td><input type="text" name="no_telpon" id="no_telpon" value="<?= $smr["no_telpon"] ?>"></td>
			</tr>
			<tr>
				<td>Tempat Lahir</td>
				<td>:</td>
				<td><input type="text" name="tempat_lahir" id="tempat_lahir" value="<?= $smr["tempat_lahir"] ?>"></td>
			</tr>
			<tr>
				<td>Tanggal Lahir</td>
				<td>:</td>
				<td><input type="date" name="tanggal_lahir" id="tanggal_lahir" value="<?= $smr["tanggal_lahir"] ?>"></td>
			</tr>
			<tr>
				<td>Jenis Instansi</td>
				<td>:</td>
				<td><select name="jenis_instansi" id="jenis_instansi">
    			<option name="jenis_instansi" value="Badan khusus">Badan Khusus</option>
    			<option name="jenis_instansi" value="Negara">Negara</option>
    			<option name="jenis_instansi" value="Swasta">Swasta</option>
			    <option name="jenis_instansi" value="Lainnya">Lainnya</option>
 				</select></td>
 			</tr>
 			<tr>
				<td>Nama Instansi</td>
				<td>:</td>
				<td><input type="text" name="nama_instansi" id="nama_instansi" required value="<?= $smr["nama_instansi"] ?>"></td><br><br>
			</tr>
		<tr>
			<td></td>
			<td></td>
			<td><input type="submit" value="Update">
		</tr>

	</table>
</form>
</body>
</html>

</body>
</html>