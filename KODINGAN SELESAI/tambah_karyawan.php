<?php 
require 'function.php';

// cek apakah tombol submit sudah ditekan atau belum
if ( isset($_POST["submit"]) ) {

	// cek apakah data berhasil di tambahkan atau tidak
	if( tambah($_POST) > 0 ) {
		echo "
			<script>
				alert('data berhasil ditambahkan!');
				document.location.href = 'laporan_karyawan.php';
			</script>
		";
	} else {
		echo "
			<script>
				alert('data gagal ditambahkan!');
				document.location.href = 'laporan_karyawan.php'.;
			</script>
		";
		echo mysqli_error($conn);
	}
}

?>


<!DOCTYPE html>
<html>
<head>
	<title>Input Karyawan</title>
</head>
<body>
	<h2>Form Input Karyawan</h2>
	<hr>
	<form action="simpan_karyawan.php" method="POST">
	<table>
		<tr>
			<td>ID</td>
			<td>:</td>
			<td><input type="text" name="id"></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td><input type="text" name="nama"></td>
		</tr>
		<tr>
			<td>Email</td>
			<td>:</td>
			<td><input type="text" name="email"></td>
		</tr>
		<tr>
			<td>No Telpon</td>
			<td>:</td>
			<td><input type="text" name="no_telpon"></td>
		</tr>
		<tr>
			<td>Tempat Lahir</td>
			<td>:</td>
			<td><input type="text" name="tempat_lahir"></td>
		</tr>
		<tr>
			<td>Tanggal Lahir</td>
			<td>:</td>
			<td><input type="date" name="tanggal_lahir"></td>
		</tr>
		<tr>
			<td>Jenis Instansi</td>
			<td>:</td>
			<td><select name="jenis_instansi">
				<option value="Badan khusus">Badan Khusus</option>
				<option value="Negara">Negara</option>
				<option value="Swasta">Swasta</option>
				<option value="Lainnya">Lainnya</option>
			</select> </td>
		</tr>
		<tr>
			<td>Nama Instansi</td>
			<td>:</td>
			<td><input type="text" name="nama_instansi"></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><input type="submit" value="Simpan"> 
				<input type="reset" value="Batal"></td>
		</tr>
	</table>
</form>
</body>
</html>